Image originally from a character artist for BIONICLE: The Game ("Chris") who
worked alongside artist Patrick Ward.

Posted on the MoD/BMP Staff Discord on Jan 3, 2019 by Hexadecimal Mantis, who
got in touch with Chris.

Original message from Chris:

"""
Hi,

Yes, I worked on Bionicle as a character artist along side artist Patrick Ward. He was my mentor back then as it was my first industry job. I think we modelled about 130 odd characters/modular models back then between us. The character concepts we're drawn up by Richard Aidely.

Argonaut Sheffield was previously Particle Systems who made I-War, the PC Sci Fi game and some other iterations. It was a technically adept small team and great to work with. I didn't have much to do with the London branch. We went on to try and make some failed movie tie-ins alongside them at a later date. Catwoman, Zorro, Star Wars, Charlie And The Chocolate Factory and also Bionicle 2.. which was going to be a fluid parkour type platformer. Which never materialised as after being there just shy of two years, London shut us down.

I'm actually working back in the offices where it all happened now. Which seems strange. I did work at Sumo Digital as a lead Char artist for ten years in between. I know there's a basement full of hard drives still here as one of the old directors still rents some space here.

[image]

Some pics from Bionicle 2

[image]

Hope this helps.

Cheers,
Chris
"""
