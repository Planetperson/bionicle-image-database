URL: https://www.artstation.com/artwork/G8LzKV

Description:

Gukko Bird Concept (Bionicle: Mask of Light)

I designed the Gukko Bird for the movie "Bionicle: Mask of Light". I did 3 versions of the Gukko Bird in all. LEGO liked the design enough to make a toy for it.

My work was done through Creative Capers Entertainment which did a majority of the design work for the film and sequels. It was a great project.

"Bionicle: Mask of Light", "Gukko Bird" © LEGO Corporation
