URL: https://www.artstation.com/artwork/5Ba0qW

Description:

Transporter (Bionicle: Mask of Light)

The Transporter was the hero vehicle used in the movie. LEGO had already designed the toy and needed the design tweaked for the film. The color image was created with markers and touched up in Photoshop.

The control drawing of the vehicle was made for the overseas modeling team at CGCG Inc. and created in Adobe Illustrator.

My work was done through Creative Capers Entertainment which did a majority of the design work for the film and sequels. It was a great project.

"Bionicle: Mask of Light" © LEGO Corporation
