URL: https://www.artstation.com/artwork/Ar4odq

Description:

Rahkshi Concept (Bionicle: Mask of Light)

I did some character design work for the "Bionicle: Mask of Light" movie. LEGO already had the toys designed before hand and need some adjustments to adapt their designs for the movie.

The second and third designs (head and Kraata details) were not used and are not official to the film. They were concerned that the designs were "too scary" which with hindsight I have to agree with. I think I might have went a bit overboard with trying to rationalize the biological and mechanical aspects of the Rahkshi.

My work was done through Creative Capers Entertainment which did a majority of the design work for the film and sequels. It was a great project.

"Bionicle: Mask of Light", "Rahkshi", "Kraata" © LEGO Corporation
