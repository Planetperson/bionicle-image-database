These images were found on the home page of Michael Rose, who worked on the Creative Capers BIONICLE movies.

http://www.mproseimages.com/work-1#/weg/

The images were downloaded at the highest possible resolution by modifying the URL of each image. Setting
`format=2500w` at the end of the URL serves the biggest version of the image.
