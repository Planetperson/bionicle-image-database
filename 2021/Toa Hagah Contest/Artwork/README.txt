This artwork won the Toa Hagah canonization contest on the TTV Message Boards in December 2021 and was canonized by Greg Farshtey as representing the official appearance of the four remaining Toa Hagah besides Norik and Iruini. The announcement video can be found here: https://www.youtube.com/watch?v=q-NcsTMSRms

The original artwork entry by specterL can be found here: https://board.ttvchannel.com/t/the-toa-hagah-bionicle-canon-contest-3-honor-guard-part-2/62156

Only the image showing all six Toa Hagah was canonized.
