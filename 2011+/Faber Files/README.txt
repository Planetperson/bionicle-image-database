These are images from Christian Faber's blog at http://faberfiles.blogspot.com
downloaded in the fullest resolution available.

Simply using right click + save as on the images in the blog will not download
them at their fullest resolution. If you look at the URL of the image, there is
a part that says "s1600". This number determines the resolution at which the
image is served. Changing it to a really high number, like "s15000", will cause
the image to be served in its full resolution. The highest number possible
without resulting in a 500 error seems to be "s16383".
