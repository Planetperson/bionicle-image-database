Downloaded from DeviantArt, user JTSilversmith.

https://www.deviantart.com/jtsilversmith/art/Charger-287609705

Image description:
A group of photos that I submitted to a design contest for the Lego group to design and build a character for the Bionicle book "The Dark Hunters Guide". This model was one of about 30 that were chosen as winners. I sent the model in so it could be photographed for inclusion in the official Lego guidebook.
