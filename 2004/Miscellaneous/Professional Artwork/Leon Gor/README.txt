URL: https://www.artstation.com/artwork/QnwxAE

Description:

Po-Metru Cog Moutain (Bionicle 2: Legends of Metru-Nui)

Here is some environmental design work I did for the "Bionicle 2: Legends of Metru-Nui" movie. I worked on the first 2 movies and had the opportunity to design characters, vehicles, environments, and props.

My work was done through Creative Capers Entertainment which did a majority of the design work for the film and sequels. It was a great project.

"Bionicle 2: Legends of Metru-Nui" ©LEGO Corporation
