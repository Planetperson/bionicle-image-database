import argparse
import pathlib
import urllib.parse

import attr
import bs4
import requests

def get_document(url, strainer=None):
    res = requests.get(url)
    return bs4.BeautifulSoup(res.text, 'html.parser', parse_only=strainer)

@attr.s
class Entry:
    url = attr.ib()
    name = attr.ib()

@attr.s
class Status:
    continue_from_url = attr.ib()
    do_continue = attr.ib()

def parse_entries(doc):
    for a in doc.select('table:nth-child(4) td > a'):
        href = a['href']
        name = str(a.parent.select_one('font font').text)
        yield Entry(href, name)

def parse_gallery(doc):
    entries = parse_entries(doc)
    a = doc.select_one('table:nth-child(5) td:nth-child(3) a')
    if a is None:
        next_page = None
    else:
        next_page = a['href']
    return entries, next_page

def process_gallery(url, output, domain, status):
    output.mkdir(exist_ok=True)
    entries, next_page = parse_gallery(get_document(url))
    for entry in entries:
        parsed_url = urllib.parse.urlparse(entry.url)
        if parsed_url.query.startswith('f='):
            process_gallery(domain + entry.url, output / entry.name, domain, status)
        elif parsed_url.query.startswith('i='):
            process_file(domain + entry.url, output, domain, status)
        else:
            raise ValueError
    if next_page is not None:
        process_gallery(domain + next_page, output, domain, status)

def process_file(url, output, domain, status):
    doc = get_document(url)
    href = doc.select_one('table:nth-child(4) a')['href']
    full_url = domain + href
    if not status.do_continue:
        status.do_continue = full_url == status.continue_from_url
    if not status.do_continue:
        print(f'skipping {full_url}')
        return
    parsed_url = urllib.parse.urlparse(href)
    name = parsed_url.path.rsplit('/', 1)[-1]
    output_path = output / name
    print(f'{full_url} => {output_path}')
    data = requests.get(full_url).content
    output_path.write_bytes(data)

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument('gallery')
    parser.add_argument('output', type=pathlib.Path)
    parser.add_argument('--continue-from')
    args = parser.parse_args()

    parsed_url = urllib.parse.urlparse(args.gallery)
    domain = f'{parsed_url.scheme}://{parsed_url.netloc}'
    status = Status(
        continue_from_url=args.continue_from,
        do_continue=args.continue_from is None
    )
    process_gallery(args.gallery, args.output, domain, status)

if __name__ == '__main__':
    main()
