import argparse
import collections
import datetime
import hashlib
import pathlib
import re

def list_recursively(dir_path, ignore):
    for path in dir_path.iterdir():
        if ignore(path):
            continue
        if path.is_dir():
            for subpath in list_recursively(path, ignore):
                yield subpath
        else:
            yield path

def ignore_func(path):
    return path.name.startswith('.git') or path.name == '.venv' or path.suffix == '.txt'

def hash_file(path):
    hasher = hashlib.sha256()
    hasher.update(path.read_bytes())
    return hasher.digest()

DATE_RE = re.compile(r'^BS01 Upload Date: (.*?)$', re.MULTILINE)

def get_file_date(path):
    comment = path.with_suffix('.txt').read_text()
    date_str, = DATE_RE.search(comment).groups()
    return datetime.datetime.strptime(date_str, '%Y-%m-%d %H:%M:00')

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument('directory', type=pathlib.Path)
    args = parser.parse_args()

    hashes = collections.defaultdict(list)
    for path in list_recursively(args.directory, ignore_func):
        hashes[hash_file(path)].append(path)

    for path_list in hashes.values():
        if len(path_list) > 1:
            path_list.sort(key=lambda x: get_file_date(x), reverse=True)
            print('duplicates:', ' '.join(map(str, path_list)))
            for dupe_path in path_list[1:]:
                dupe_txt_path = dupe_path.with_suffix('.txt')
                print('deleting', dupe_path)
                dupe_path.unlink()
                print('deleting', dupe_txt_path)
                dupe_txt_path.unlink()

if __name__ == '__main__':
    main()
