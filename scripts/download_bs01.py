import argparse
import pathlib

import attr
import bs4
import datetime
import requests

DOMAIN = 'https://biosector01.com'
FIRST_URL = 'https://biosector01.com/wiki/Special:ListFiles?limit=20&ilsearch=&user='
PAGE_STRAINER = bs4.SoupStrainer(id='mw-content-text')
FILE_STRAINER = bs4.SoupStrainer(class_='wikitable filehistory')

OUTPUT_DIR = pathlib.Path(__file__).parent / 'bs01'

def get_page(url, strainer=None):
    res = requests.get(url)
    return bs4.BeautifulSoup(res.text, 'html.parser', parse_only=strainer)

def process_page(url):
    doc = get_page(url, PAGE_STRAINER)
    rows = doc.select('.TablePager tbody tr')
    for row in rows:
        file_url = DOMAIN + row.select_one('.TablePager_col_img_name > a')['href']
        process_file(file_url)
    next_url = doc.select_one('.TablePager_nav > span:nth-child(3) > a').get('href')
    if next_url is not None:
        next_url = DOMAIN + next_url
    return next_url

DATE_FORMAT = '%H:%M, %d %B %Y'

@attr.s
class FileVersion:
    date = attr.ib()
    url = attr.ib()
    user = attr.ib()
    comment = attr.ib()

IGNORED_EXTENSIONS = set(['.mp3'])

def process_file(url):
    _, base_name = url.rsplit('/File:', 1)
    base_name = pathlib.Path(base_name)
    if base_name.suffix.lower() in IGNORED_EXTENSIONS:
        return
    doc = get_page(url, FILE_STRAINER)
    file_versions = list(list_file_versions(doc))
    file_versions = [
        x
        for x in file_versions
        if not (x.comment is not None and x.comment.startswith('Reverted to version as of '))
    ]
    file_versions.reverse()
    for i, f in enumerate(file_versions, 1):
        file_name = base_name
        if len(file_versions) > 1:
            file_name = file_name.stem + f' - {i}' + file_name.suffix
        print(file_name)
        file_path = OUTPUT_DIR / file_name
        full_url = DOMAIN + f.url
        file_path.write_bytes(requests.get(full_url).content)
        comment_file_path = file_path.with_suffix('.txt')
        comment_parts = []
        if f.comment:
            comment_parts.append(f.comment)
            comment_parts.append('')
        comment_parts.append(f'BS01 Upload Date: {f.date}')
        comment_parts.append(f'BS01 URL: {full_url}')
        comment_parts.append(f'BS01 Uploader: {f.user}')
        comment_file_str = ''.join(s + '\n' for s in comment_parts)
        comment_file_path.write_text(comment_file_str)

def list_file_versions(doc):
    rows = doc.select('tr')[1:]
    for row in rows:
        a = row.select_one(':scope > :nth-child(2) > a')
        date_str = a.string
        date = datetime.datetime.strptime(date_str, DATE_FORMAT)
        file_url = a['href']
        if len(row.select(':scope > *')) == 6:
            # Image file
            user = row.select_one(':scope > :nth-child(5) bdi').string
            comment = row.select_one(':scope > :nth-child(6)').string
        else:
            # PDF file
            user = row.select_one(':scope > :nth-child(4) bdi').string
            comment = row.select_one(':scope > :nth-child(5)').string
        yield FileVersion(date, file_url, user, comment)

def remove_prefix(s, prefix):
    if not s.startswith(prefix):
        raise ValueError
    return s[len(prefix):]

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument('--first-url', default=FIRST_URL)
    args = parser.parse_args()

    url = args.first_url
    while url is not None:
        print(url)
        url = process_page(url)
        break

if __name__ == '__main__':
    main()
