import argparse
import pathlib

INVALID_CHARS = '<>:"/\\|?*'

def list_recursively(dir_path, ignore):
    for path in dir_path.iterdir():
        if ignore(path):
            continue
        if path.is_dir():
            for subpath in list_recursively(path, ignore):
                yield subpath
        else:
            yield path

def ignore_func(path):
    return path.name.startswith('.git') or path.name == '.venv' or path.suffix == '.txt'

def get_new_path(path):
    parent = path.parent
    stem = path.stem
    ext = path.suffix
    stem = stem.replace(INVALID_CHARS, '_')
    ext = ext.lower()
    if ext == '.jpeg':
        ext = '.jpg'
    return parent / (stem + ext)

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument('directory', type=pathlib.Path)
    args = parser.parse_args()

    for path in list_recursively(args.directory, ignore_func):
        new_path = get_new_path(path)
        if path != new_path:
            print(f'{path} -> {new_path}')
            path.rename(new_path)
        txt_path = path.with_suffix('.txt')
        new_txt_path = new_path.with_suffix('.txt')
        if txt_path != new_txt_path:
            print(f'{txt_path} -> {new_txt_path}')
            txt_path.rename(new_txt_path)

if __name__ == '__main__':
    main()
