import argparse
import hashlib
import pathlib

def list_recursively(dir_path, ignore):
    for path in dir_path.iterdir():
        if ignore(path):
            continue
        if path.is_dir():
            for subpath in list_recursively(path, ignore):
                yield subpath
        else:
            yield path

def ignore_func(path):
    return path.name.startswith('.git') or path.name == '.venv'

def bs01_ignore_func(path):
    return path.suffix == '.txt'

def hash_file(path):
    hasher = hashlib.sha256()
    hasher.update(path.read_bytes())
    return hasher.digest()

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument('main_dir', type=pathlib.Path)
    parser.add_argument('bs01_dir', type=pathlib.Path)
    args = parser.parse_args()

    hashes = {}
    for path in list_recursively(args.main_dir, ignore_func):
        hashes[hash_file(path)] = path

    for path in list_recursively(args.bs01_dir, bs01_ignore_func):
        file_hash = hash_file(path)
        dupe_file = hashes.get(file_hash)
        if dupe_file is not None:
            print(f'{path} is a copy of {dupe_file}')
            path.unlink()
            txt_path = path.with_suffix('.txt')
            txt_path.unlink()
            #print(txt_path.read_text())

if __name__ == '__main__':
    main()
