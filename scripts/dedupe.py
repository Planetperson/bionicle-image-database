import argparse
import os
import pathlib
import subprocess

def list_recursively(dir_path):
    for path in dir_path.iterdir():
        if path.is_dir():
            for subpath in list_recursively(path):
                yield subpath
        else:
            yield path

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument('directory', type=pathlib.Path)
    parser.add_argument('primary', type=pathlib.Path)
    parser.add_argument('--confirm', action='store_true', default=False)
    args = parser.parse_args()

    primary_hashes = {}
    for path in list_recursively(args.primary):
        hash_value = hash(path.read_bytes())
        primary_hashes[hash_value] = path

    for path in list_recursively(args.directory):
        hash_value = hash(path.read_bytes())
        primary_path = primary_hashes.get(hash_value)
        if primary_path is not None:
            cmds = []
            if path.suffix == '.PSD':
                cmds.append(['rm', str(path)])
                path = path.with_suffix('.psd')
            elif path.suffix == '.EPS':
                cmds.append(['rm', str(path)])
                path = path.with_suffix('.eps')
            primary_path = pathlib.Path(os.path.relpath(primary_path, path.parent))
            cmds.append(['ln', '-sf', str(primary_path), str(path)])
            for cmd in cmds:
                if args.confirm:
                    subprocess.run(cmd)
                else:
                    print(cmd)

if __name__ == '__main__':
    main()
