# BIONICLE Image Database

This is a comprehensive database of
[BIONICLE](https://en.wikipedia.org/wiki/Bionicle)
images managed by [the Great Archives](https://thegreatarchives.com/).
The goal of this project is to preserve and catalog all known official images
tied to LEGO's BIONICLE theme (mainly the 2001-2010 run) in the best
resolutions available. To make images easy to find, all images are organized
by story year and category and can be annotated with comments. The images are
stored in a
[git](https://en.wikipedia.org/wiki/Git)
repository, which allows us to preserve the edit history of the database and
review changes before merging them into the "master" version.

## Contributing

Have you found images that aren't in this database? Feel free to add them!
There are a few different ways to do this:

1. Easiest: Join the Great Archives [Discord server](https://discord.gg/RB5wWjf)
   and leave a message in the #image-database channel. We'll take care of the
   rest and make sure you get credit for the find.
2. Through GitLab's website: Create a [GitLab](https://gitlab.com/) account and
   add your images by making a
   [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
   to the [BIONICLE Image Database GitLab repository](https://gitlab.com/Planetperson/bionicle-image-database).
   You can automatically create a merge request simply by
   [uploading a new image through GitLab's web interface](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html#new-merge-request-by-adding-editing-and-uploading-a-file)
   and then selecting the option to create a new merge request to the "master"
   branch at the end. Make sure to upload the image in the appropriate folder
   and give it an appropriate name (see below).
3. Most advanced (best for uploading many files at once): Clone the whole git
   repository to your computer using
   [Git](https://en.wikipedia.org/wiki/Git) and
   [Git LFS](https://git-lfs.github.com/),
   then commit your image files and create a new merge request using
   [these instructions](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html#new-merge-request-from-your-local-environment).

## Downloading

You can view all of the files in the database at the
[web page on GitLab](https://gitlab.com/Planetperson/bionicle-image-database),
but this is not the most convenient way, since it does not include thumbnails.
For easier browsing, it is recommended to download the database to your
computer, where you can look at the images in your file browser.

It is possible to simply
[download a zip file](https://gitlab.com/Planetperson/bionicle-image-database/-/archive/master/bionicle-image-database-master.zip)
of the database through the GitLab web page. This works well for viewing the
files once, but if there are ever changes to the database, you will need to
download a new zip file containing the entire database all over again. You also
can't contribute your own changes this way.

The most flexible way to download the database is to clone the GitLab
repository using
[Git](https://en.wikipedia.org/wiki/Git) and
[Git LFS](https://git-lfs.github.com/).
This will allow you to download new changes incrementally, without
re-downloading everything you downloaded the first time. It will also give you
the most flexible way to contribute changes directly to the repository.

Git is what's known as a version control system (VCS). It's usually used for
managing computer code for software development projects, but thanks to the
Git LFS (large file support) plugin, it works just as well for images. Using
git is very helpful because it allows us to maintain a permanent record of all
changes to the database and keep track of who contributed which changes. We
can also review changes before merging them into the official "master" branch
of the repository. Finally, the repository can also easily be hosted for free
on GitLab. Thanks GitLab!

See instructions below for installing Git and Git LFS, setting up symlinks, and
cloning the repository.

### Cloning on Windows

If you want to clone the repository to Windows, download
[Git for Windows](https://git-scm.com/download/win).
In the setup options, make sure that Git LFS and Symlinks are enabled. Git LFS
should be enabled by default, but Symlinks is not and needs to be checked.
Afterwards, you can clone the repository to a new directory (e.g. using the
Git GUI). You can use
`https://gitlab.com/Planetperson/bionicle-image-database.git`
as the URL to clone from.

### Problem with Links on Windows

There are some files in the database that are actually just *links* to other
image files in the database. For example, all of the Faber Files images are in
one directory, but for the sake of organization, many other folders link to
these files. These links are called symbolic links or symlinks. Using links
saves a lot of space, because we don't need to store multiple copies of the
same image.

The problem is that if you try to open one of these links in Windows, it will
probably give you an error message (something like "It looks like we don't
support this file format"), because what is named like an image file is
actually just a text file with a path to another file. Don't worry -- this
problem is normal, and you can find out the path to the original image by
opening the link in text pad. Alternatively, you can get Windows to
conveniently open the original file for you when you open the link by following
[these instructions](https://github.com/git-for-windows/git/wiki/Symbolic-Links#allowing-non-administrators-to-create-symbolic-links)
to enable symlinks on Windows.

### Setting up Git and Git LFS on MacOS

Setting up Git and Git LFS is relatively simple on MacOS. Use
[these instructions](https://git-scm.com/download/mac)
to install Git, and use
[these instructions](https://docs.github.com/en/repositories/working-with-files/managing-large-files/installing-git-large-file-storage)
to install Git LFS. Make sure to follow step 6 and run the command
`git lfs install` in Terminal before cloning `bionicle-image-database`.
Afterwards, clone `bionicle-image-database` as described above.

## Folder Organization

Images are roughly organized by the story year of the subject matter they
depict, and then further organized by type (whether it is an image of a set,
artwork of a scene from the storyline, an image with a character against a
blank background, etc.).

Here is a breakdown of common sub-folders for each year:

* Scenes - Official artwork, usually made by Advance, that depicts
  "storyboard" scenes from the storyline (like the ones found in the old
  instruction booklets)
* Sets - Official artwork of sets set against some sort of illustrated
  background (like the ones used as box art or the Bios on bionicle.com)
* No Background - Official images of sets against a blank
  (white/black/transparent) background
* Packaging - Images of packaging for the sets
* Comics - Images of comics, which can be scans of physical comics or files
  with just the artwork without any text
* Collectibles - Images of collectibles such as masks, krana, kraata, etc.
* Assets - Miscellaneous images, sprites, or other files extracted from games,
  animations, websites, etc.
* Miscellaneous - Things that don't really go anywhere else; the sub-folders
  within it are organized by source if possible

## Images with Multiple Versions

Many images have multiple versions. These versions often come from different
sources and show the same thing but with different levels of quality, different
resolutions, or different text overlays. Multiple versions of the same image
are stored as multiple files. The first part of the names of these files should
all be the same, followed by space-hyphen-space (` - `), followed by the name
of the version, which can be any descriptive piece of text you choose. For
example, these files are all different versions of the image named `Tahu`:

* `Tahu - Bio.jpg`
* `Tahu - Gold CD.bmp`
* `Tahu - Good.jpg`
* `Tahu - Style Guide.png`
* `Tahu - Wallpaper.jpg`

If there is only one version of an image, then no ` - ` and version name is
necessary, and just a simple name may be used.

## Annotating Things with Comments

Folders, images, and specific image versions can all be annotated with
comments. This is done by adding text (`.txt`) files with matching names.

* To comment a folder, create a file in the folder named `README.txt`
* To comment all versions of an image (e.g. all versions of `Tahu`), use the
  base name (e.g. `Tahu.txt`)
* To comment a specific version of an image (e.g. `Tahu - Bio.jpg`), just
  replace the file extension with `.txt` (e.g. `Tahu - Bio.txt`), or add the
  `.txt` extension to the original file name (e.g. `Tahu - Bio.jpg.txt`). The
  second option is useful when there are multiple files with the same names
  but different extensions.

Good things to include in comments are explanations of where images were found,
who illustrated them, etc. This could include links to archived web pages on
`web.archive.org`, a professional artist's home page, etc.

Comment files can include data fields, each on a separate line, to indicate
common pieces of data, like URLs or artists.

Here is what a comment file might look like:

```
This is an image of Takua.

URL: http://example.com/
Artists: Christian Faber
Original Filename: takuaaa.png
```

## Other Conventions

* Sometimes it makes sense to include the same image file in multiple places.
  When doing so, in order to save space, do not make copies of the file, but
  instead symlink to just one instance of the file. Symlinking requires running
  special commands on Linux, Mac, and Windows.
* All file names must be cross-platform compatible and should not contain
  characters that are not allowed on Windows. Specifically, they cannot
  contain the following characters: `<>:"/\|?*`.
